package main
import (
	"go-web-native/config"
	"net/http"
	"log"
)

func main(){
	config.ConnectDB()
	log.Println("Server running on port 8080")
	http.ListenAndServe(":8080",nil)
}